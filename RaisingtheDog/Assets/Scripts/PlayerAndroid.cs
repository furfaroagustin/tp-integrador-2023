using UnityEngine;
using UnityEngine.UI;

public class PlayerAndroid : MonoBehaviour
{
    public float movementSpeed = 3;
    public float jumpForce = 300;
    public float timeBeforeNextJump = 1.2f;
    private float canJump = 0f;
    private Animator anim;
    private Rigidbody rb;
    public Button botonIzquierda;
    public Button botonDerecha;
    public Button botonSalto;

    // Variables para controlar el estado de los botones
    private bool isPressingIzquierda = false;
    private bool isPressingDerecha = false;
    private bool isPressingSalto = false;

    void Start()
    {
        anim = GetComponent<Animator>();
        rb = GetComponent<Rigidbody>();
    }

    void Update()
    {
        ControlPlayer();
    }

    void ControlPlayer()
    {
        // Obtener la direcci�n del movimiento (izquierda o derecha) usando botones en el canvas
        float moveHorizontal = 0f;

        if (isPressingIzquierda)
            moveHorizontal = -1f;
        else if (isPressingDerecha)
            moveHorizontal = 1f;

        // Controlar la animaci�n del jugador seg�n el movimiento
        if (moveHorizontal != 0f)
        {
            transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.LookRotation(new Vector3(moveHorizontal, 0.0f, 0.0f)), 0.15f);
            anim.SetInteger("Walk", 1);
        }
        else
        {
            anim.SetInteger("Walk", 0);
        }

        // Mover el jugador en la direcci�n del movimiento
        transform.Translate(new Vector3(moveHorizontal * movementSpeed * Time.deltaTime, 0f, 0f), Space.World);

        // Controlar el salto utilizando un bot�n en el canvas
        if (isPressingSalto && Time.time > canJump)
        {
            rb.AddForce(0, jumpForce, 0);
            canJump = Time.time + timeBeforeNextJump;
            anim.SetTrigger("jump");
        }
    }

    // Estos m�todos se llaman desde los botones en el canvas para cambiar el estado de los botones
    public void OnButtonDownIzquierda()
    {
        isPressingIzquierda = true;
    }

    public void OnButtonUpIzquierda()
    {
        isPressingIzquierda = false;
    }

    public void OnButtonDownDerecha()
    {
        isPressingDerecha = true;
    }

    public void OnButtonUpDerecha()
    {
        isPressingDerecha = false;
    }

    public void OnButtonDownSalto()
    {
        isPressingSalto = true;
    }

    public void OnButtonUpSalto()
    {
        isPressingSalto = false;
    }
}
