using UnityEngine;

public class CameraFollow : MonoBehaviour
{
    public Transform target;    // Referencia al objeto del jugador que seguir� la c�mara
    public float smoothSpeed = 0.125f;    // Velocidad suavizada de movimiento de la c�mara

    // Puedes ajustar la posici�n de la c�mara usando un offset para darle un desplazamiento
    public Vector3 offset;

    void LateUpdate()
    {
        // Comprobamos que el objetivo (jugador) no sea nulo
        if (target == null)
        {
            Debug.LogWarning("El objetivo de la c�mara es nulo.");
            return;
        }

        // Calculamos la nueva posici�n de la c�mara, suavizando el movimiento
        Vector3 desiredPosition = target.position + offset;
        Vector3 smoothedPosition = Vector3.Lerp(transform.position, desiredPosition, smoothSpeed);

        // Asignamos la nueva posici�n a la c�mara
        transform.position = smoothedPosition;

        // Aseguramos que la c�mara mire hacia el jugador
        transform.LookAt(target);
    }
}
